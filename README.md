# wpscan-ci

CI to scan an WordPress site with the famous tool **wpscan** 

You can retrieve the result scan in a web browser through gitlab pages.

## Usage 

It's turnkey. All you need to do is to setup your target in the `gitlab-ci.yml` via the `TARGET` variable. 

This following setup will scan example.com WordPress site :

```
Scan Wordpress:
  stage: scan
  image:
    name: wpscanteam/wpscan
    entrypoint: [""]
  variables:
    TARGET: "https://example.com"
  script:
    - wpscan --url ${TARGET} -e vp,vt,tt,cb,dbe,u,m --format json --output scan_result.json
  artifacts:
    name: "WordPress Scan Report ${CI_COMMIT_SHA}"
    paths:
      - scan_result.json
    expire_in: 1 hour
```
