import json
from jinja2 import Environment, FileSystemLoader

# Function to convert JSON file to HTML
def json_to_html(input_json, output_html, template_file):
    with open(input_json, 'r') as json_file:
        data = json.load(json_file)

    # Config Jinja2 environment to load template from a file
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template(template_file)

    # Generate template with JSON data
    html_output = template.render(data=data)

    # Load rendered HTML to the output file
    with open(output_html, 'w') as html_file:
        html_file.write(html_output)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Convert JSON to HTML")
    parser.add_argument("input_json", help="Input JSON file")
    parser.add_argument("output_html", help="Output HTML file")
    parser.add_argument("template_file", help="Template file (e.g., template.html)")

    args = parser.parse_args()

    json_to_html(args.input_json, args.output_html, args.template_file)